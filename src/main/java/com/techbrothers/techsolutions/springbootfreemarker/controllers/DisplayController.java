/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techbrothers.techsolutions.springbootfreemarker.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author chiru
 */
@Controller
public class DisplayController {
    
    @Value("${userFirstName}")
    private String userFirstName;
    
    @Value("${userLastName}")
    private String userLastName;
    
    
    @GetMapping("/display")
    public String display(Model model){
        model.addAttribute("firstName", userFirstName);
        model.addAttribute("lastName", userLastName);
        return "display";
    }
    
}
